
public class RunCases {

	static TestCases test = new TestCases();
	 
	public static void main(String[] args) {
		test.DriverConnection();	
		
		boolean radioButton = test.validateRadioButtons();
		System.out.println(radioButton);
		
		boolean suggestion = test.validateSuggestions();
		System.out.println(suggestion);
		
		boolean checkboxValidation = test.validateCheckbox();
		System.out.println(checkboxValidation);
		
		boolean uncheck = test.UncheckFirstCheckbox();
		System.out.println(uncheck);
		
		boolean hideElement = test.validatedHide();
		System.out.println(hideElement);
		
		
		boolean showElement = test.validatedShow();
		System.out.println(showElement);
		
		boolean table = test.validateWebTableHeader();
		System.out.println(table);
		
		boolean amountTotal = test.validateTotalAmount();
		System.out.println(amountTotal);
		
		boolean findFrame = test.validateIframe();
		System.out.println(findFrame);
		
	}

}
