import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class TestCases {
	
	//String variable
	String country = "South";
	public static WebDriver driver;
	
	
	//-	Connections
	public void DriverConnection() {
		System.setProperty("webdriver.chrome.driver", "C:\\Development\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://rahulshettyacademy.com/AutomationPractice/ ");
        driver.manage().window().maximize();

	}
	
	//-	Radio buttons
	public boolean validateRadioButtons() {
		boolean valid = false;
		List <WebElement> radioButtons = driver.findElements(By.name("radioButton"));
		
		this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		radioButtons.get(2).click();
		
		if((!radioButtons.get(0).isSelected())&&(!radioButtons.get(1).isSelected())&&(radioButtons.get(2).isSelected())) {
			valid = true;
		}
		return valid;	
	}
	
	//-	Auto Complete
	public boolean validateSuggestions() {
		driver.findElement(By.id("autocomplete")).sendKeys(country);
		this.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"ui-id-1\"]/li[2]")).click();
		String autoText = driver.findElement(By.id("autocomplete")).getText();
		if ( autoText == "South Africa") {
			return true;
		}
		return false;
	}
	
	// - Checkboxes
	public boolean validateCheckbox() {
		boolean allChecked = false;
		List <WebElement> checkbox = driver.findElements(By.xpath("//*[@type=\"checkbox\"]"));
		checkbox.get(0).click();
		this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		checkbox.get(1).click();
		this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		checkbox.get(2).click();
		this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		if((checkbox.get(0).isSelected())&&(checkbox.get(1).isSelected())&&(checkbox.get(2).isSelected())) {
			allChecked = true;
		}
		return allChecked;
	}
	
	// - uncheck 1st checkbox
	public boolean UncheckFirstCheckbox() {
		boolean allChecked = false;
		List <WebElement> checkbox = driver.findElements(By.xpath("//*[@type=\"checkbox\"]"));
		this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		checkbox.get(0).click();
		
		
		if((!checkbox.get(0).isSelected())&&(checkbox.get(1).isSelected())&&(checkbox.get(2).isSelected())) {
			allChecked = true;
		}
		return allChecked;
	}
	
	// - validate hidden textbox
	public boolean validatedHide() {
		boolean elementVisible = false;
		this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(driver.findElement(By.xpath("//*[@name='show-hide']")).isDisplayed()) {
			this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("hide-textbox")).click();
			
			
			if(!driver.findElement(By.xpath("//*[@name='show-hide']")).isDisplayed()) {	
				elementVisible = true;
			}
		}
		return elementVisible;
	}	
	
	// - validate showen textbox
	public boolean validatedShow() {
		boolean elementVisible = false;
		this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(!driver.findElement(By.xpath("//*[@name='show-hide']")).isDisplayed()) {
			this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("show-textbox")).click();
			
			
			if(driver.findElement(By.xpath("//*[@name='show-hide']")).isDisplayed()) {	
				elementVisible = true;
			}
		}
		return elementVisible;
	}	
		
		
	// --validate table data
	public boolean validateWebTableHeader() {
		boolean validateTableData = false;
		List <WebElement> tableData = driver.findElements(By.xpath("/html/body/div[3]/div[2]/fieldset[2]/div[1]/table/tbody/tr[6]/td"));
		String name = tableData.get(0).getText();
		String position = tableData.get(1).getText();
		String city = tableData.get(2).getText();
		String amount = tableData.get(3).getText();
		
		if((name.equals("Joe"))&&(position.equals("Postman"))&&(city.equals("Chennai"))&&(amount.equals("46"))) {
			validateTableData = true;
		}
		
		return validateTableData;
	}
	
	// --validate total amount
	public boolean validateTotalAmount() {
		boolean validateTotal = false;
		
		
		String amount = driver.findElement(By.className("totalAmount")).getText();
		if(amount.equals("Total Amount Collected: 296")) {
			validateTotal = true;
		}
		
		return validateTotal;
		
	}
		
	
	
	// --iFrame
	public boolean validateIframe() {
		boolean frame = false;
		
		if(driver.findElement(By.id("courses-iframe")).isDisplayed()) {
		
			frame = true;
		}
		return frame;
	}		
}
